package com.maaii.test.common.messages;

public class TestMessage extends AbstractMaiiMessage {

  private int messageNumber;
  private int messagesAmount;
  private byte[] payload;

  public TestMessage() {
  }

  public TestMessage(int i, int scheduledMessages, byte[] payload) {
    this.messageNumber = i;
    this.messagesAmount = scheduledMessages;
    this.payload = payload;
  }

  public int getMessageNumber() {
    return messageNumber;
  }

  public void setMessageNumber(int messageNumber) {
    this.messageNumber = messageNumber;
  }

  public int getMessagesAmount() {
    return messagesAmount;
  }

  public void setMessagesAmount(int messagesAmount) {
    this.messagesAmount = messagesAmount;
  }

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(byte[] payload) {
    this.payload = payload;
  }
}
