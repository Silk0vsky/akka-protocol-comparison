package com.maaii.test.common.messages;

import java.util.Date;

public class TransmissionFinished extends AbstractMaiiMessage {

  public enum Reason { SUCCESS, TIMEOUT}

  private long started;
  private long finished;
  private long messagesReceived;
  private Reason reason;

  public TransmissionFinished() {
  }

  public TransmissionFinished(Date started, Date finished, int messagesReceived, Reason reason) {
    this.started = started.getTime();
    this.finished = finished.getTime();
    this.messagesReceived = messagesReceived;
    this.reason = reason;
  }

  public long getStarted() {
    return started;
  }

  public void setStarted(long started) {
    this.started = started;
  }

  public long getFinished() {
    return finished;
  }

  public void setFinished(long finished) {
    this.finished = finished;
  }

  public long getMessagesReceived() {
    return messagesReceived;
  }

  public void setMessagesReceived(long messagesReceived) {
    this.messagesReceived = messagesReceived;
  }

  public Reason getReason() {
    return reason;
  }

  public void setReason(Reason reason) {
    this.reason = reason;
  }
}