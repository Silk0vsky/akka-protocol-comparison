package com.maaii.test.common.utils;

import com.google.gson.Gson;

public class TestUtils {

  private static Gson gson = new Gson();

  public static void sleep(int millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public static String toJson(Object object) {
    return gson.toJson(object);
  }

}
