package com.maaii.test.common.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorPath;
import akka.japi.pf.ReceiveBuilder;
import com.maaii.test.common.messages.TestMessage;

public class RemoteReceiverActor extends AbstractActor {

  public RemoteReceiverActor() {
      ActorPath path = self().path();
      System.out.println("RemoteReceiverActor created: " + path.address() + " " + path.name());
  }

  @Override
  public Receive createReceive() {
      return ReceiveBuilder.create()
              .match(TestMessage.class, msg -> {
                  ActorPath path = self().path();
//                  System.out.println("incoming message: " + msg.getMessageNumber() + "; \t" + path.address() + " " + path.name());
                  sender().tell(msg.getMessageNumber(), self());
              })
              .build();
  }

}