package com.maaii.test;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import com.maaii.test.actors.LoadGeneratorActor;
import com.maaii.test.actors.SenderActor;
import com.maaii.test.common.actors.RemoteReceiverActor;
import com.maaii.test.common.utils.TestUtils;
import com.maaii.test.dto.TestState;
import com.typesafe.config.Config;

import java.util.Date;

public class AkkaService {

  private ActorSystem system;
  private ActorRef loadGenerator;


  public void startActorSystem(Config config) {
    system = ActorSystem.create("sender-system", config);
    loadGenerator = system.actorOf(Props.create(LoadGeneratorActor.class), "load-generator");
    system.actorOf(FromConfig.getInstance().props(Props.create(RemoteReceiverActor.class)), "receiverRouter");
    system.actorOf(FromConfig.getInstance().props(Props.create(SenderActor.class)), "sendersRouter");
  }

  public TestState runTest(int messagesAmount, int payloadBytesSize) {

    TestState testState = new TestState(messagesAmount, payloadBytesSize);
    loadGenerator.tell(testState, ActorRef.noSender());

    boolean[] acknowledges = testState.getAcknowledges();
    while (!acknowledges[acknowledges.length - 1]) {
      TestUtils.sleep(10);
    }

    while (!allTrue(acknowledges)) {
      TestUtils.sleep(10);
    }
    testState.setFinished(new Date().getTime());

    System.out.println(" > messages sent: " + messagesAmount);
    System.out.println(" > execution time: " + (testState.getFinished() - testState.getStarted()));
    return testState;
  }

  private boolean allTrue(boolean[] array) {
    for (int i = 0; i < array.length; i++) {
      if (!array[i])
        return false;
    }
    return true;
  }

}