package com.maaii.test.dto;

import lombok.Data;

@Data
public class TestResult {

    private Long executionMillis;
    private Integer messages;
    private Integer payload;

    public TestResult(TestState testState) {
        this.executionMillis = testState.getFinished() - testState.getStarted();
        this.messages = testState.getScheduledMessages();
        this.payload = testState.getPayloadBytesSize();
    }

}
