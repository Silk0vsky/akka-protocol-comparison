package com.maaii.test.dto;

import lombok.Data;

@Data
public class TestState {

  private Long started;
  private Long finished;
  private Long executionMillis;
  private Integer scheduledMessages;
  private Integer payloadBytesSize;
  private boolean[] acknowledges;

  public TestState(Integer scheduledMessages, Integer payloadBytesSize) {
    this.scheduledMessages = scheduledMessages;
    this.payloadBytesSize = payloadBytesSize;
    this.acknowledges = new boolean[scheduledMessages];
  }

}