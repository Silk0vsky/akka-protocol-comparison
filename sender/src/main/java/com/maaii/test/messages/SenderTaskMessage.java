package com.maaii.test.messages;

import com.maaii.test.common.messages.TestMessage;
import com.maaii.test.dto.TestState;
import lombok.Data;

@Data
public class SenderTaskMessage {

    private TestState testState;
    private TestMessage testMessage;

    public SenderTaskMessage() {
    }
    public SenderTaskMessage(TestState testState, TestMessage testMessage) {
        this.testState = testState;
        this.testMessage = testMessage;
    }

}
