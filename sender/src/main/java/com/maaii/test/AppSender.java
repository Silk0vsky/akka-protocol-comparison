package com.maaii.test;

import com.maaii.test.common.utils.TestUtils;
import com.maaii.test.dto.TestResult;
import com.maaii.test.dto.TestState;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import spark.Spark;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class AppSender {

  private static AkkaService akkaService = new AkkaService();

  public static void main(String[] args) {
    Map<String, String> arguments = Arrays.asList(args).stream()
            .collect(Collectors.toMap(arg -> arg.split("=")[0], arg -> arg.split("=")[1]));
    System.out.println("Starting with arguments: " + arguments);

    Config config = prepareAkkaConfig(arguments);
    akkaService.startActorSystem(config);

    System.out.println(config.root().render());

    configureRestServer(akkaService);
  }

  private static Config prepareAkkaConfig(Map<String, String> arguments) {
    String mode = arguments.getOrDefault("mode", "netty");
    Config config = ConfigFactory.load("akka-" + mode);
    System.out.println("mode: " + mode);

    return config;
  }

  private static void configureRestServer(AkkaService akkaService) {
    Spark.setPort(8080);
    Spark.get("/test", (req, res) -> {
      res.type("application/json");

      String amount = req.queryParams("amount");
      int messagesAmount = amount != null? Integer.valueOf(amount): 10;
      String payloadBytesSize = req.queryParams("payloadBytesSize");
      int payloadSize = payloadBytesSize != null? Integer.valueOf(payloadBytesSize): 100;

      TestState testState = akkaService.runTest(messagesAmount, payloadSize);
      return new TestResult(testState);
    }, TestUtils::toJson);
  }

}