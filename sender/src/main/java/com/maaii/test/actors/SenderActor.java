package com.maaii.test.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;
import com.maaii.test.messages.SenderTaskMessage;

public class SenderActor extends AbstractActor {

    private ActorSelection remoteReceiverRouter;
    private boolean[] acknowledges;

    public SenderActor() {
        remoteReceiverRouter = context().actorSelection("/user/receiverRouter");
        System.out.println("SenderActor created");
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(SenderTaskMessage.class, task -> {
                    this.acknowledges = task.getTestState().getAcknowledges();      // <- ok for the same task
                    remoteReceiverRouter.tell(task.getTestMessage(), self());
                })
                .match(Integer.class, acknowledgeId -> {
//                    System.out.println("ack id: " + acknowledgeId);
                    acknowledges[acknowledgeId] = true;
                })
                .build();
    }

}