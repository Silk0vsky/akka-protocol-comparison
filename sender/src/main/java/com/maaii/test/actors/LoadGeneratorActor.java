package com.maaii.test.actors;

import com.maaii.test.dto.TestState;
import com.maaii.test.common.messages.TestMessage;

import java.util.Date;
import java.util.Random;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;
import com.maaii.test.messages.SenderTaskMessage;

public class LoadGeneratorActor extends AbstractActor {

    private TestState testState;

    public LoadGeneratorActor() {
        System.out.println("LoadGeneratorActor created");
    }

    private void resetTestStatistic(TestState testState){
        this.testState = testState;
        testState.setStarted(new Date().getTime());
        System.out.println("transmission >> amount:" + testState.getScheduledMessages() + "; payloadSize:" + testState.getPayloadBytesSize());
    }

    private byte[] generateDummyPayload(int size) {
        byte[] payload = new byte[size];
        new Random().nextBytes(payload);
        return payload;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(TestState.class, testState -> {
                    resetTestStatistic(testState);
                    byte[] payload = generateDummyPayload(testState.getPayloadBytesSize());

                    ActorSelection sendersRouter = context().actorSelection("/user/sendersRouter");
                    for (int i = 0; i < testState.getScheduledMessages(); i++) {
                        TestMessage testMessage = new TestMessage(i, testState.getScheduledMessages(), payload);
                        sendersRouter.tell(new SenderTaskMessage(testState, testMessage), self());
                    }
                    System.out.println("transmission queued to remote system");
                })
                .build();
    }

}