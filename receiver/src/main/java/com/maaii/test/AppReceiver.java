package com.maaii.test;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class AppReceiver {

  public static void startActorSystem(Config config) {
    ActorSystem.create("receiver-system", config);
  }

  public static void main(String[] args) {
    Map<String, String> arguments = Arrays.asList(args).stream()
            .collect(Collectors.toMap(arg -> arg.split("=")[0], arg -> arg.split("=")[1]));
    System.out.println("Starting with arguments: " + arguments);

    String mode = arguments.getOrDefault("mode", "netty");
    System.out.println("mode: " + mode);

    Config config = ConfigFactory.load("akka-" + mode);
    System.out.println(config.root().render());
    startActorSystem(config);
  }

}